Matt Payne, [13.07.19 14:29]
https://chocolatey.org/packages/jdk8 and friends... finding a list now.

Matt Payne, [13.07.19 14:30]
WSL, node, VSCode, eclipse, gradle, maven, angular

Matt Payne, [13.07.19 14:32]
Virtual Machine Software 

 

For the labs, assignments and projects, it will be necessary for software to be installed on the virtual machines we’ll use. 

 

Use this document as the place to provide instructions (screen prints, etc.) for how to access and install software in a VM. 

 

In Matt_1 I have done these things: 

Upgraded Windows 10 so I could install Windows Subsystem for Linux (WSL) 

Installed WSL so students can practice with a Linux (Ubuntu) system inside their VM 

Installed, in WSL, TMUX https://hackernoon.com/a-gentle-introduction-to-tmux-8d784c404340 because it’s very handy and popular. 

TODO: Install, in WSL, tig - https://jonas.github.io/tig/ -- also very handy 

Installed https://sdkman.io/ in the Ubuntu.   Used it to install Java 11.   Suggest WSL has Java 11 and Windows has Java 8.   This will let students easily try both Java 11 and Java 8. 

Installed Java 8 (JDK) on Windows. 

Installed Eclipse IDE for Enterprise Java Developers (Version 2019-03 (4.11.0) – this is just the latest Eclipse for JEE.   Will be used for both java and part of the servlet work.  During servlet work we’ll show IntelliJ and (I suggest) let people choose (IntelliJ encouraged). 

TODO: Install IntelliJ 

TODO: Install https://infinitest.github.io/ in both eclipse & IntelliJ (people can disable it) 

Installed git bash because it’s useful and very popular in Omaha companies 

Installed node 10.16, then angular (npm install -g @angular/cli) 

Installed VSCode because it’s a joy to use when writing angular applications

Matt Payne, [13.07.19 14:33]
those are my notes from when I did a rush job a while back.

Matt Payne, [13.07.19 14:33]
It would be good to install the things system wide.


Matt Payne, [13.07.19 15:14]
mysql too
